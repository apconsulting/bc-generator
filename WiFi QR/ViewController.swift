//
//  ViewController.swift
//  WiFi QR
//
//  Created by Ilya Kharebashvili on 22.11.2018.
//  Copyright © 2018 Ilya Kharebashvili. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration.CaptiveNetwork
import NotificationCenter


var type:String="WPA"
var img = CIImage()
var img2 = CIImage()
var effect:UIVisualEffect!
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


class ViewController: UIViewController, UITextFieldDelegate
{
    
    @IBOutlet weak var popUpO: UIButton!
    @IBOutlet weak var wifiO: UIImageView!
    @IBOutlet weak var closeO: UIButton!
    @IBOutlet weak var generateO: UIButton!
    @IBOutlet var addItemView: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    @IBOutlet weak var name: UITextField!
  //  @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var SwitchName: UISegmentedControl!
    
    @IBOutlet weak var myImageView: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        name.addTarget(self, action: #selector(close), for: .editingDidEndOnExit)
//        password.addTarget(self, action: #selector(close), for: .editingDidEndOnExit)
        closeO.isHidden = true
        /*
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
 
 */
        name.delegate = self
//        password.delegate = self
        
        popUpO.layer.cornerRadius = 10
        generateO.layer.cornerRadius = 10
        
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        
        addItemView.layer.cornerRadius = 5
        
        
        
    }
    
    
    

    @IBAction func closeKey(_ sender: Any)
    {
        name.resignFirstResponder()
       // password.resignFirstResponder()
    }
    
    @IBAction func generate(_ sender: Any)
    {
        
        if((name.text?.isEmpty)!)
        {
            let alert = UIAlertController(title: "Error", message: "Enter barcode text", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
        
         generateO.isHidden = true
         name.isHidden = true
         SwitchName.isHidden = true
         wifiO.isHidden = true
         animateIn()
         myImageView.image = generateBarcode(from: "\(name.text!)")
         
        }
 
        
        
        
        
        
    }
    
    @IBAction func switcher(_ sender: Any)
    {
        
    }
    
    
    // ANIMATIONS
    
    
    func animateIn() {
        self.view.addSubview(addItemView)
        addItemView.center = self.view.center
        
        addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        addItemView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = effect
            //self.visualEffectView.effect = self.effect
            self.addItemView.alpha = 1
            self.addItemView.transform = CGAffineTransform.identity
        }
        
    }
    
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self.addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.addItemView.alpha = 0
            
            self.visualEffectView.effect = nil
            
        }) { (success:Bool) in
            self.addItemView.removeFromSuperview()
        }
    }
    
    //
    
    func displayQRCodeImage() {
        let scaleX = myImageView.frame.size.width / img.extent.size.width
        let scaleY = myImageView.frame.size.height / img.extent.size.height
        
        let transformedImage = img.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        img2 = transformedImage
        myImageView.image = UIImage.init(ciImage: transformedImage)
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    @IBAction func PupUpButton(_ sender: Any)
    {
        //shareImage()
        
        share(shareImage: myImageView.image!)
        
        animateOut()
        
        generateO.isHidden = false
        name.isHidden = false
       // password.isHidden = false
        SwitchName.isHidden = false
        wifiO.isHidden = false
 
    }
    
    // SHARE
    
    func share(shareImage:UIImage?){
        
        var objectsToShare = [AnyObject]()
        if let shareImageObj = shareImage{
            objectsToShare.append(shareImageObj)
        }
        
        if shareImage != nil{
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            present(activityViewController, animated: true, completion: nil)
        }else{
            print("There is nothing to share")
        }
    }
    
    
    
    
    func shareImage()
    {
        
        var sharedImage = UIImage(named: "wifi.png")
        sharedImage = myImageView.image!
        let items = [sharedImage]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        
        /*var sharedImage = UIImage()
        sharedImage = myImageView.image!
        
        let objectsToShare = [sharedImage]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: [])
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: screenWidth/2, y: screenHeight-65, width: 64, height: 64)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true)
 */
    }
    // ////////////
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            closeO.isHidden = false
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        closeO.isHidden = true

    }
    
    @objc func close()
    {
        name.resignFirstResponder()
//        password.resignFirstResponder()
        
    }
    
    func generateBarcode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                let context:CIContext = CIContext.init(options: nil)
                let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
                let image:UIImage = UIImage.init(cgImage: cgImage)
                return image
                //eturn UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
}

